﻿/*
 * Rachel Culver
 * CS 481
 * Homework 5 - Map
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace map_02
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        Pin pin1;
        Pin pin2;
        Pin pin3;
        Pin pin4;

        Position position1;
        Position position2;
        Position position3;
        Position position4;
       
        //Const. initalizes pins and picker items 
        public MainPage()
        {
            InitializeComponent();
            Init_Pins();

            MainPicker.Items.Add("Tea Cup Cattle");
            MainPicker.Items.Add("Jauregui & Culver Inc.");
            MainPicker.Items.Add("St. Stephen's Catholic Church");
            MainPicker.Items.Add("San Diego Zoo Safari Park");


        }

        //When location from picker menu is selected user is moved to location where that pin will be found
        private void MainPicker_OnSelectedIndexChange(object sender, EventArgs e)
        {
            var name = MainPicker.Items[MainPicker.SelectedIndex];
            if (name == "Tea Cup Cattle")
            {
                myMap.MoveToRegion(MapSpan.FromCenterAndRadius(position1, Distance.FromMiles(1)));
            }
            else if (name == "Jauregui & Culver Inc.")
            {
                myMap.MoveToRegion(MapSpan.FromCenterAndRadius(position2, Distance.FromMiles(1)));
            }
            else if (name == "St. Stephen's Catholic Church")
            {
                myMap.MoveToRegion(MapSpan.FromCenterAndRadius(position3, Distance.FromMiles(1)));
            }
            else if (name == "San Diego Zoo Safari Park")
            {
                myMap.MoveToRegion(MapSpan.FromCenterAndRadius(position4, Distance.FromMiles(1)));
            }
        }

        //Initializes pins and adds them to map
        public void Init_Pins()
        {
            position1 = new Position(33.1375596, -116.9714107);
            position2 = new Position(33.125798, -117.099239);
            position3 = new Position(33.275434, -117.024615);
            position4 = new Position(33.104849, -116.992987);

            pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Tea Cup Cattle",
                Address = "www.teacupcattle.com"
            };

            pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Jauregue & Culver Inc.",
                Address = "www.jcincorp.net",
                

            };

            pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "St. Stephen's Catholic Church",
                Address = "ststephenvc.com"
            };

            pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "San Diego Zoo Safari Park",
                Address = "sdzsafaripark.org"
            };

            myMap.Pins.Add(pin1);
            myMap.Pins.Add(pin2);
            myMap.Pins.Add(pin3);
            myMap.Pins.Add(pin4);
        }

        //Sets map to street view
        private void Street_Clicked(Object sender, EventArgs e)
        {
            myMap.MapType = MapType.Street;
        }

        //Sets map to satallite view
        private void Satallite_Clicked(Object sender, EventArgs e)
        {
            myMap.MapType = MapType.Satellite;
        }
    }
}
